$(function(){
	$('.reviews-sliders-inner').slick({
		infinite: true,
	  	slidesToShow: 2,
  		slidesToScroll: 2,
  		arrows: false,
  		dots: true
	});
});